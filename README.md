# Gerador de relação de pagamentos

Script para gerar relação de pagamentos de um estabelecimento comercial. 

É necessário criar dois arquivos, um para as contas da pessoa jurídica/pessoa física e outro para os feriados do ano.

# Tecnologias utilizadas 
- Pandas (Manipulação de dados)
- Tabulate (Formatação de dados)