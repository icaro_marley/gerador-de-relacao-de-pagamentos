'''
Script para gerar relação de pagamentos
    > o dia/mês é calculado automaticamente com o dia em que o Script é executado
    > vencimentos que não são fixos são recalculados para dias após fds e feriados 
    > caso o dia de pagamento caia antes do início do mês, a conta será ignorada
    > finais de semana e feriádos são destacados na saída

entradas: 
    > arquivo csv de feriados do ano (ver feriados.csv fonte http://www.feriados.com.br/feriados-aracaju-se.php)
    DATA data em formato dd/mm/yyy
    NOME nome do feriado

    > arquivo csv de contas de um mês genérico (ver contas.csv)
    DIA APROX. vencimento em formato dd/mm/yyy
    CONTA nome da conta
    AÇÕES como conseguir a conta (não é utilizado no script)
    PERÍODO lista de meses para pagar (por padrão, todos)
    DIA FIXO se o vencimento não é alterado em caso de FDS ou feriados. Formato S/N

saída: 
    relação de vencimentos e contas, nome do arquivo segue o formato mm_yyy.txt  (ver 04_2019.txt)
    
    
ele ta rodando do dia 3    
'''

import datetime
import pandas as pd
import copy

# processamento do dia
today = datetime.datetime.now() # testes datetime.datetime.now().replace(month=6)
year = today.year
month = today.month # 1 janeiro
week_day = today.replace(day=1).weekday() #0 segunda
month_day = today.day 
month_end = (today.replace(month=month+1, day=1) - datetime.timedelta(days=1)).day

file_path = 'feriados.csv'
holydays = pd.read_csv(file_path,parse_dates=[0],date_parser=lambda x:datetime.datetime.strptime(x,'%d/%m/%Y'))

file_path = 'contas.csv' 
bills = pd.read_csv(file_path)
# processamento das contas
bills['DIA APROX.'] = bills['DIA APROX.'].astype(int)
bills['DIA FIXO'] = (bills['DIA FIXO'] == 'S').astype(bool)
bills['PERÍODO'] = bills['PERÍODO'].apply(lambda x:str(x).strip().replace('nan',''))
bills['PERÍODO'] = bills['PERÍODO'].apply(lambda x:list(range(1,13)) if x=='' else [int(x_) for x_ in x.split(',')])

last_valid_day = month_end

no_pay_days = {day:[] for day in range(1,month_end+1)} # marca feriados e finais de semana
for day in no_pay_days.keys():
    week_day_aux = (week_day + day - 1) % 7
    if week_day_aux > 4: # final de semana
        if week_day_aux == 5:
            no_pay_days[day].append('SÁBADO')
        else:
            no_pay_days[day].append('DOMINGO')
    if not holydays[(holydays['DATA'].dt.day==day) & (holydays['DATA'].dt.month==month)].empty:
        no_pay_days[day] += (holydays[(holydays['DATA'].dt.day==day) & (holydays['DATA'].dt.month==month)]['NOME']).str.upper().values.tolist()
    if no_pay_days[day]:
        no_pay_days[day][0] = '---' + no_pay_days[day][0]

days_bills = copy.deepcopy(no_pay_days)
for day in range(1,32):
    bills_day = bills[bills['DIA APROX.'] == day]
    for index,bill in bills_day.iterrows():
        pay_day_bill = day
        if month not in bill['PERÍODO']: continue
        rollback = False
        
        while True:
            try:
                if not no_pay_days[pay_day_bill]:
                    break
            except:
                pass
            if not bill['DIA FIXO']: # calcular shift positivo
                if not rollback:
                    pay_day_bill += 1
                    if pay_day_bill > month_end: # passou do fim do mês, shift negativo
                        rollback = True
                        pay_day_bill = day - 1
                else:
                    pay_day_bill += -1
            else: # shift negativo
                pay_day_bill += -1
            if pay_day_bill <= 0: # rollback passou do ínicio do mês, conta deveria ter sido paga no mês passado
                break
        if pay_day_bill > 0:
            days_bills[pay_day_bill].append(bill['CONTA'])
             
with open('{:02d}_{:}.txt'.format(month,year),"w") as f:
    f.write('RELAÇÃO DE PAGAMENTOS MÊS {:02d}/{}'.format(month,year)+'\n')
    f.write('GERADA DIA {:02d}/{:02d}/{}'.format(month_day,month,year)+'\n\n')
    for day in days_bills.keys():
        f.write('{}\t{} '.format(day,', '.join(days_bills[day])).expandtabs(3)+'\n')